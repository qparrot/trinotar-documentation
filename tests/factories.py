import random
from uuid import uuid4

import factory
from factory import fuzzy

from toobibfree.schema.fhir.r4.datatype.code import AddressUseEnum
from toobibfree.schema.fhir.r4.datatype.datatypes import Address, Identifier
from toobibfree.schema.fhir.r4.resource.organization import Organization


class IdentifierFactory(factory.Factory):
    class Meta:
        model = Identifier

    id = uuid4()
    value = factory.Faker("ssn")
    system = factory.Faker("url")


class AddressFactory(factory.Factory):
    class Meta:
        model = Address

    id = uuid4()
    use = fuzzy.FuzzyChoice([e.value for e in AddressUseEnum])
    city = factory.Faker("city")
    postalCode = factory.Faker("postcode")


class OrganizationFactory(factory.Factory):
    class Meta:
        model = Organization

    id = uuid4()
    address = [AddressFactory() for _ in range(1, random.randint(1,4))]
    identifier = [IdentifierFactory() for _ in range(1, random.randint(1,2))]
    name = factory.Faker("company")
    partOf = uuid4()
