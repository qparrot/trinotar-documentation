import os
import pprint as pp

import pytest
from sqlalchemy.orm import Session

from tests.factories import OrganizationFactory


@pytest.mark.skipif(
    os.environ.get("GITLAB_CI") == "true", reason="cannot download container on CI"
)
def test_postgres(db_session: Session):
    res = db_session.execute("select 1").first()
    assert res == (1,)

def test_organization_factory():
    orga = OrganizationFactory.build().dict()
    pp.pprint(orga)
    assert orga["id"]
    assert orga["identifier"]
