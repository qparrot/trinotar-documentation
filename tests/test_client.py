""" see https://flask.palletsprojects.com/en/1.1.x/testing/#the-testing-skeleton """
import json


def test_get_organization(client):
    result = client.get("api/Organization/a11fe247-84d0-46f7-90ca-e513551da9bf")
    result_dict = result.json
    assert result.status_code == 200
    assert result_dict["id"]


def test_post_organization(client):
    json_data = """{
  "address": [
    {
      "city": "paris",
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "postalCode": "75000",
      "use": "home"
    }
  ],
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "identifier": [
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "system": "http://interhop.org/identifiers",
      "value": "xxb2-ha"
    }
  ],
  "name": "Hospital X",
  "partOf": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}    """
    dict_data = json.loads(json_data)
    result = client.post("api/Organization", json=dict_data)
    result_dict = result.json

    assert result.status_code == 200
    assert result_dict == dict_data
