from uuid import UUID

from pydantic import BaseModel, Field


class Resource(BaseModel):
    id: UUID = Field(..., description="the ressource unique identifier")
