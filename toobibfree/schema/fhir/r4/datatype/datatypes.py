from typing import Optional
from uuid import UUID

from pydantic import BaseModel, HttpUrl

from toobibfree.schema.fhir.r4.datatype.code import AddressUseEnum


class Element(BaseModel):
    id: UUID


class Identifier(Element):
    value: str
    system: HttpUrl


class Address(Element):
    use: AddressUseEnum
    city: Optional[str]
    postalCode: Optional[str]
