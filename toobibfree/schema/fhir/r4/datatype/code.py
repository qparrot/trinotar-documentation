from enum import Enum


# str is important for serialization
class AddressUseEnum(str, Enum):
    home = "home"
    work = "work"
