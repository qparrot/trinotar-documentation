from flask import Flask

from toobibfree.api.api_organization import organization
from toobibfree.schema.spec import api

app = Flask(__name__)
api.register(app)
app.register_blueprint(organization)

if __name__ == "__main__":
    app.run(port=8000)
