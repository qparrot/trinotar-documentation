[![PyPI Version][pypi-image]][pypi-url]
[![Build Status][build-image]][build-url]
[![Code Coverage][coverage-image]][coverage-url]
[![Code Quality][quality-image]][quality-url]
...
<!-- Badges -->
[pypi-image]: https://img.shields.io/pypi/v/toobibfree
[pypi-url]: https://pypi.org/project/toobibfree/
[build-image]: https://framagit.org/interhop/toobibfree-py/actions/workflows/build.yml/badge.svg
[build-url]: https://framagit.org/interhop/toobibfree-py/actions/workflows/build.yml
[coverage-image]: https://codecov.io/gh/interhop/toobibfree-py/branch/main/graph/badge.svg
[coverage-url]: https://codecov.io/gh/interhop/toobibfree-py
[quality-image]: https://api.codeclimate.com/v1/badges/3130fa0ba3b7993fbf0a/maintainability
[quality-url]: https://codeclimate.com/framagit/interhop/toobibfree-py

# Toobib-FREE (or not Toobib)

## Documentation

Visit the [TOOBIB-FREE
DOCUMENTATION](https://interhop.frama.io/toobib/toobib-free/), build
with mkdoc and ci-cd (see `.gitlab-ci.yml`)


## Install

```
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
pip3 install -r requirements-dev.txt
PYTHONPATH=./ python3 toobibfree/app.py
# go to http://127.0.0.1:8000/apidoc/swagger

```


## Auto generated Openapi

- [Auto generated openapi](http://127.0.0.1:8000/apidoc/swagger)


## Build

```
tox 
```

## Production Docker Install


```bash
docker-compose up -d
# check http://localhost/apidoc/swagger
```
[Docker image source](https://github.com/tiangolo/meinheld-gunicorn-docker)

# Readings

- [spectree documentation](https://0b01001001.github.io/spectree/)
- [pydantic documentation](https://pydantic-docs.helpmanual.io/)
- [spectree example](https://github.com/0b01001001/spectree/blob/master/examples/flask_demo.py)
- [pydantic VS marshmallow](https://www.augmentedmind.de/2020/10/25/marshmallow-vs-pydantic-python/)
- [great demo project](https://github.com/luolingchun/flask-api-demo)

