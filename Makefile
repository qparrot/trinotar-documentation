.DEFAULT_GOAL := help
.PHONY: coverage deps help lint push test
help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
coverage:  ## Run tests with coverage
	pytest --cov=toobibfree --cov-report=term-missing -vv tests	
deps:  ## Install dependencies
	pip install -r requirements-dev.txt
lint:  ## Lint and static-check
	isort --atomic  toobibfree tests
	black toobibfree tests
	pylint toobibfree
	mypy toobibfree
push:  ## Push code with tags
	git push && git push --tags
test:  ## Run tests
	pytest -ra
docs:  ## Build the docs
	mkdocs build
	mkdocs serve
